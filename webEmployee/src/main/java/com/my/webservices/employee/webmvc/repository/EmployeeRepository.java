package com.my.webservices.employee.webmvc.repository;
import com.my.webservices.employee.webmvc.domain.Employee;
import org.springframework.roo.addon.layers.repository.jpa.RooJpaRepository;

@RooJpaRepository(domainType = Employee.class)
public interface EmployeeRepository {
}
