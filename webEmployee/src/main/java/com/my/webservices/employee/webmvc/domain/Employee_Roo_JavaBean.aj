// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.my.webservices.employee.webmvc.domain;

import com.my.webservices.employee.webmvc.domain.Employee;
import java.util.Date;

privileged aspect Employee_Roo_JavaBean {
    
    public String Employee.getFirstName() {
        return this.firstName;
    }
    
    public void Employee.setFirstName(String firstName) {
        this.firstName = firstName;
    }
    
    public String Employee.getLastName() {
        return this.lastName;
    }
    
    public void Employee.setLastName(String lastName) {
        this.lastName = lastName;
    }
    
    public String Employee.getSuName() {
        return this.suName;
    }
    
    public void Employee.setSuName(String suName) {
        this.suName = suName;
    }
    
    public Date Employee.getBirthDate() {
        return this.birthDate;
    }
    
    public void Employee.setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }
    
    public String Employee.getDepartment() {
        return this.department;
    }
    
    public void Employee.setDepartment(String department) {
        this.department = department;
    }
    
    public String Employee.getPhone() {
        return this.phone;
    }
    
    public void Employee.setPhone(String phone) {
        this.phone = phone;
    }
    
    public String Employee.getEmail() {
        return this.email;
    }
    
    public void Employee.setEmail(String email) {
        this.email = email;
    }
    
}
