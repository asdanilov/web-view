package com.my.webservices.employee.webmvc.service;
import org.springframework.roo.addon.layers.service.RooService;

@RooService(domainTypes = { com.my.webservices.employee.webmvc.domain.Employee.class })
public interface EmployeeService {
}
