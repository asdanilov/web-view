// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.my.webservices.employee.webmvc.repository;

import com.my.webservices.employee.webmvc.domain.Employee;
import com.my.webservices.employee.webmvc.repository.EmployeeRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

privileged aspect EmployeeRepository_Roo_Jpa_Repository {
    
    declare parents: EmployeeRepository extends JpaRepository<Employee, Long>;
    
    declare parents: EmployeeRepository extends JpaSpecificationExecutor<Employee>;
    
    declare @type: EmployeeRepository: @Repository;
    
}
