// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.my.webservices.employee.webmvc.domain;

import com.my.webservices.employee.webmvc.domain.EmployeeDataOnDemand;
import org.springframework.beans.factory.annotation.Configurable;

privileged aspect EmployeeDataOnDemand_Roo_Configurable {
    
    declare @type: EmployeeDataOnDemand: @Configurable;
    
}
